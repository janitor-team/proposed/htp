#!/usr/bin/perl
#
# pretty print htp code.
#
# options: 
#  -n include line numbers
#  -s include EBNF like syntax

require "hlhtp.pl";


while ($ARGV[0] =~ /^-(.*)/) {
    $_ = $1;
    $optn = 1 if (/^n$/i);
    $opts = 1 if (/^s$/i);
    shift @ARGV;
}


print "<p><code>\n";
while (<>) {
    $_ = parsehtml($_);

    if ($optn) {
	# add line header
	$head = sprintf '<font size="-2">%2d. </font>', ++$linenr;
	$head =~ s/> />&nbsp;/g;
	$_ = "$head$_";
    }

    chomp $_;
    print "$_<br>\n";
}
print "</code></p>\n";
