<BLOCK syntax>
<IF [NOT] conditional>
    block #1
[
<ELSEIF [NOT] conditional>
    block #2
...
]
[
<ELSE>
    block #3
]
</IF>
</BLOCK>

<BLOCK synopsis>

<STRONG>IF, NOT, ELSEIF, ELSE, and /IF</STRONG> are the building
blocks for conditional processing.  The basic form is shown above.
<P> "Conditional" is either a "compare" or "is defined" operation.  A
compare operator is a test of the value of a macro against a literal
string:

<htpcode>
    <IF name="Jim">...</IF>
</htpcode>
<P>
and a defined operator is a test of a macro's existance:
<P>
<htpcode>
    <IF name>...</IF>
</htpcode>
<P>
If the NOT tag is present in the IF markup, the evaluation of the
conditional is reversed.  The first IF or ELSEIF condition that
evaluates to true is chosen.  If none of the conditions is true the
ELSE block is taken if it is present.  A conditional block
<EM>must</EM> be closed with the /IF tag.  IF tags can be nested.

</BLOCK>

