#
# makefile - htp
#
# HTML pre-processor
# Copyright (c) 2002 Jochen Hoenicke
#

include Makefile.config

d:=.

DISTDIRS  :=
DISTFILES := Makefile Makefile.config CLARIFIED_ARTISTIC INSTALL
CLEAN     := *~
SCRUB     := $(MAIN)-*.zip $(MAIN)-*.tar.gz
HTML      :=

# sub directories

dir:=src
include $(dir)/Makefile.sub
dir:=tests
include $(dir)/Makefile.sub
dir:=homepage
include $(dir)/Makefile.sub
dir:=examples
include $(dir)/Makefile.sub

all:
.PHONY: all install dist scrub clean force 

scrub:: clean
	rm -f $(SCRUB)

clean::
	rm -rf $(RELDIR)
	rm -f $(CLEAN)

dist:
	rm -rf $(RELDIR)
	for i in $(DISTDIRS); do mkdir -p $(RELDIR)/$$i; done
	for i in $(DISTFILES); do cp -p $$i $(RELDIR)/$$i; done

release: all release-src release-doc release-bin
	ls -l $(MAIN)-$(VERSION)*
	@test "$(RELEASE)" = 1 || \
        echo "****** ADD RELEASE=1 TO Makefile.config *******"

release-src: dist
	tar -czf $(MAIN)-$(VERSION).tar.gz $(RELDIR)
	rm -rf $(RELDIR)
	@test "$(RELEASE)" = 1 || \
        echo "****** ADD RELEASE=1 TO Makefile.config *******"

release-doc: $(HTP)
	rm -rf $(RELDIR)
	$(MAKE) install-doc pkgdocdir=`pwd`/$(RELDIR)
	zip -r $(MAIN)-$(VERSION)-doc.zip $(RELDIR)
	rm -rf $(RELDIR)

release-bin: $(HTP)
	$(STRIP) $(HTP)
	zip -j $(MAIN)-$(VERSION)-$(OS).zip $(HTP) CLARIFIED_ARTISTIC


$(HTML): %.html: %.htp $(HTP)
	cd $(dir $<); $(PWD)/$(HTP) -quiet -nodepend $(notdir $<) $(notdir $@)

vars:
	@echo HTML=$(HTML)
	@echo HTMLhomepage=$(HTMLhomepage)
	@echo HTMLhomepage/ref=$(HTMLhomepage/ref)
	@echo HTMLtests=$(HTMLtests)
	@echo HTMLexamples=$(HTMLexamples)
	@echo SRC=$(SRC)
	@echo HTP=$(HTP)
