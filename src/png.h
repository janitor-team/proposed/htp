/*
//
// png.h
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef PNG_H

#define PNG_H

/* PNG format detected? */
BOOL PngFormatFound(FILE *file);

/* get image dimensions */
BOOL PngReadDimensions(FILE *file, DWORD *height, DWORD *width);

#endif

