/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
//
// misc-proc.c
//
// specialized markup processors
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#include "misc-proc.h"

#include "defs.h"
#include "option.h"

uint HeadProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext)
{
    UNREF_PARAM(htmlMarkup);
    UNREF_PARAM(newPlaintext);

    /* authors ego-gratifying easter egg */
    /* put a generator meta-tag at the end of the HTML header */
    StreamPrintF(task->outfile, 
                 "<meta name=\"Generator\" content=\"%s %s\"",
                 PROGRAM_NAME, VER_STRING);
    if(IsOptionEnabled(OPT_I_XML)) {
        StreamPrintF(task->outfile, " />\n");
    } else {
        StreamPrintF(task->outfile, ">\n");
    }
    return MARKUP_OKAY;
}


uint HtpCommentProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext)
{
    UNREF_PARAM(task);
    UNREF_PARAM(htmlMarkup);
    UNREF_PARAM(newPlaintext);

    return DISCARD_MARKUP;
}

uint QuoteProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext)
{
    HTML_ATTRIBUTE *attrib;
    UNREF_PARAM(task);
    UNREF_PARAM(newPlaintext);

    attrib = htmlMarkup->attrib;
    if (attrib == NULL || attrib->value != NULL) {
        HtpMsg(MSG_ERROR, task->infile, "improper QUOTE syntax");
        return MARKUP_ERROR;
    }
    FreeMemory(htmlMarkup->tag);
    if (htmlMarkup->whitespace)
        FreeMemory(htmlMarkup->whitespace);
    htmlMarkup->tag = attrib->name;
    htmlMarkup->whitespace = attrib->whitespace;
    htmlMarkup->attrib = attrib->next;
    FreeMemory(attrib);

    return MARKUP_OKAY;
}



uint ConditionalWarning(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext)
{
    UNREF_PARAM(htmlMarkup);
    UNREF_PARAM(newPlaintext);

    HtpMsg(MSG_ERROR, task->infile, "IFNOT tag no longer recognized; use IF NOT instead");
    return MARKUP_ERROR;
}



uint PreProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext)
{
    UNREF_PARAM(htmlMarkup);
    UNREF_PARAM(newPlaintext);

    /* inside a <PRE>...</PRE> the CR's are important */
    if(IsMarkupTag(htmlMarkup, "PRE"))
    {
        ForceLinefeeds(task->outfile, TRUE);
    }
    else if(IsMarkupTag(htmlMarkup, "/PRE"))
    {
        ForceLinefeeds(task->outfile, FALSE);
    }
    return MARKUP_OKAY;
}


