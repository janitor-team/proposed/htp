/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
//
// set-proc.h
//
// specialized markup processors
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef SET_PROC_H
#define SET_PROC_H

#include "defs.h"

uint SetProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext);

uint UnsetProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext);

#define MAX_INC_VALUE_LENGTH (32)
uint IncProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext);

#endif
