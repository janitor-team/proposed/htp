/*
//
// os.h
//
// Operating system definitions and constants
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef OS_H
#define OS_H

#if __MSDOS__

#define PROGRAM_OS              ("DOS")

/*
// this section should (but is not guaranteed) to conform to most MS-DOS
// compilers
*/

#include <dir.h>

/* not defined in the standard include files */
typedef unsigned int            uint;
typedef unsigned long           ulong;

/* directory delimiters */
#define DIR_DELIMITER           '\\'
#define DIR_DELIMITER_STRING    "\\"
#define DIR_CURRENT_STRING      ".\\"

/* the "other" filesystem delimiter table (for conversion) */
#define OTHER_FILESYSTEM_DELIMITER  "/"

/* DOS filesystem does not support pathnames beyond 64 characters (!!) */
#define MAX_PATHNAME_LEN        (64)
#define MAX_CMDLINE_LEN         (128)
#define MAX_FILENAME_LEN        (13)

#elif defined(__WIN32__) || defined(_WIN32)

#define PROGRAM_OS              ("Win32")

/*
// this section specifically designed for Microsoft Visual C++ 4.0
*/

#include <direct.h>

/* not defined in the standard include files */
typedef unsigned int            uint;
typedef unsigned long           ulong;

/* directory delimiters */
#define DIR_DELIMITER           '\\'
#define DIR_DELIMITER_STRING    "\\"
#define DIR_CURRENT_STRING      ".\\"

/* the "other" filesystem delimiter table (for conversion) */
#define OTHER_FILESYSTEM_DELIMITER  "/"

/* Win32 filesystem supports long filenames! */
#define MAX_PATHNAME_LEN        (1024)
#define MAX_CMDLINE_LEN         (1024)
#define MAX_FILENAME_LEN        (256)

#else

#define PROGRAM_OS              ("Linux/Posix")

/*
// Linux/UNIX is the only other OS being actively developed
// other UNIX platforms should pretty much look like this, though
*/

#include <unistd.h>

#define stricmp                 strcasecmp
#define strnicmp                strncasecmp

#if __CYGWIN__
typedef unsigned long           ulong;
#endif

/* directory delimiters */
#define DIR_DELIMITER           '/'
#define DIR_DELIMITER_STRING    "/"
#define DIR_CURRENT_STRING      "./"

/* the "other" filesystem delimiter table (for conversion) */
#define OTHER_FILESYSTEM_DELIMITER  "\\"

/* max pathname for UNIX (may be more, but this is sufficient) */
/* (BTW, these values may be incorrect, but they should be sufficient. */
/* Anyone with more concrete values, please email me.) */
#define MAX_PATHNAME_LEN        (1024)
#define MAX_CMDLINE_LEN         (1024)
#define MAX_FILENAME_LEN        (256)

#endif

/*
// all supported filesystem delimiters
*/
#define ALL_FILESYSTEM_DELIMITERS   "/\\"

#endif

