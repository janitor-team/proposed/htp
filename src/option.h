/*
//
// option.h
//
// Program options and setting
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef OPTION_H
#define OPTION_H

#include "defs.h"

/* option names */
#define OPT_I_IMGXY     0
#define OPT_I_MESSAGE   1
#define OPT_I_DEPEND    2
#define OPT_I_PRECIOUS  3
#define OPT_I_CONDENSE  4
#define OPT_I_USAGE     5
#define OPT_I_DELIM     6
#define OPT_I_XML       7
#define OPT_I_COUNT     8

/* option values */
#define OPT_V_FALSE         0
#define OPT_V_TRUE          1
#define OPT_V_SEMI          2
#define OPT_V_DELIM_HTML    3
#define OPT_V_DELIM_SQUARE  4
#define OPT_V_DELIM_CURLY   5

/* maximum length of an option name */
#define MAX_OPTION_LENGTH   32

extern uint *currentOptions;

/* these are used at the beginning and end of program execution, respectively */
BOOL InitializeGlobalOption(void);
void DestroyGlobalOption(void);

/* these are used at the beginning and end of local context, that is, as a new */
/* file is being created and completed */
BOOL InitializeLocalOption(void);
void DestroyLocalOption(void);

/* parse text or markups for options ... global setting dictates whether to */
/* use current context or default to global option settings */
BOOL ParseToken(TASK *task, const char *string);
uint OptionProcessor(TASK *task, 
		     HTML_MARKUP *htmlMarkup, 
		     char **newPlaintext);

/* retrieve option information ... IsOptionEnabled() doesnt mean much for */
/* options that require values rather than boolean states, and for boolean */
/* options, GetOptionValue() returns OPT_V_TRUE or OPT_V_FALSE */
#define IsOptionEnabled(i)  (currentOptions[i] == OPT_V_TRUE)
#define IsOptionValue(i,v)  (currentOptions[i] == (v))
#define GetOptionValue(i)   (currentOptions[i])

/* macros for quick lookups */
#define IMGXY               (IsOptionEnabled(OPT_I_IMGXY))
#define DEPEND              (IsOptionEnabled(OPT_I_DEPEND))
#define PRECIOUS            (IsOptionEnabled(OPT_I_PRECIOUS))
#define CONDENSE            (IsOptionValue(OPT_I_CONDENSE,OPT_V_TRUE))
#define SEMICONDENSE        (IsOptionValue(OPT_I_CONDENSE,OPT_V_SEMI))
#define USAGE               (IsOptionEnabled(OPT_I_USAGE))

#endif

