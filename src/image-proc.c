/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
//
// image-proc.c
//
// specialized markup processors
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#include "defs.h"
#include "option.h"
#include "snprintf.h"


/*
// specialized markup processors
*/

uint ImageProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext)
{
    char str[32];
    IMAGEFILE imageFile;
    DWORD width, height;
    const char *imgSource;
    const char *imgFilename;
    const char *imgText;
    char altFilename[MAX_FILENAME_LEN+8];

    UNREF_PARAM(newPlaintext);

    /* parse down the image source to just the filename */
    imgSource = MarkupAttributeValue(htmlMarkup, "SRC");
    if(imgSource == NULL)
    {
        HtpMsg(MSG_WARNING, task->infile, "image SRC not specified, skipping");
            return MARKUP_OKAY;
    }
    
    /* try to find ALT text in store */
    /* first: is there already ALT attribute?  if so, skip this step */
    if(MarkupAttributeValue(htmlMarkup, "ALT") == NULL)
    {
        /* parse the image name, just find the filename */
        imgFilename = FindFilename(imgSource);
        strcpy(altFilename, "_htpalt_");
        StringCopy(altFilename + 8, imgFilename, MAX_FILENAME_LEN);

        /* add the specified text to the image */
        imgText = GetVariableValue(task->varstore, altFilename);
        if  (imgText != NULL) {
            AddAttributeToMarkup (htmlMarkup, "alt", imgText, QUOTES_DOUBLE);
            HtpMsg(MSG_INFO, task->infile, "ALT text \"%s\" added to IMG \"%s\"",
                imgText, imgSource);
        }
    }

    /* if option is turned off, then just include the markup as-is */
    if(IMGXY == FALSE)
    {
        return MARKUP_OKAY;
    }

    /* if width and/or height are already specified, then include the */
    /* markup as-is with no modifications */
    if (MarkupAttributeValue(htmlMarkup, "HEIGHT") != NULL
        || MarkupAttributeValue(htmlMarkup, "WIDTH") != NULL)
    {
        return MARKUP_OKAY;
    }

    /* open the image file, get its dimensions, and close the file */
    if(OpenImageFile(imgSource, &imageFile) == FALSE)
    {
        HtpMsg(MSG_WARNING, task->infile, "unable to open image file \"%s\"",
            imgSource);
        return MARKUP_OKAY;
    }

    if(GetImageDimensions(&imageFile, &width, &height) == FALSE)
    {
        HtpMsg(MSG_WARNING, task->infile, "unable to determine image file \"%s\" dimensions",
            imgSource);
    }
    else
    {
        /* add the width and height specifier for the image */
        snprintf(str, 32, "%lu", width);
        AddAttributeToMarkup(htmlMarkup, "width", str, QUOTES_DOUBLE);
        snprintf(str, 32, "%lu", height);
        AddAttributeToMarkup(htmlMarkup, "height", str, QUOTES_DOUBLE);

        /* print out an informational message to the user */
        HtpMsg(MSG_INFO, task->outfile, "image file \"%s\" dimensions (%u x %u) added",
               imgSource, width, height);
    }

    CloseImageFile(&imageFile);

    /* include the markup in the final output */
    return MARKUP_OKAY;
}


uint AltTextProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext)
{
    const char *imgName, *imgText;
    char *altNameCopy, *imgTextCopy;
    char altName[MAX_FILENAME_LEN+8];

    UNREF_PARAM(newPlaintext);

    /* get the relevant information */
    imgName = MarkupAttributeValue(htmlMarkup, "NAME");

    /* requires at least a NAME parameter */
    if (imgName == NULL)
    {
        HtpMsg(MSG_ERROR, task->infile, 
               "ALTTEXT requires a nonempty NAME attribute");
        return MARKUP_ERROR;
    }
    imgText = MarkupAttributeValue(htmlMarkup, "TEXT");

    strcpy(altName, "_htpalt_");
    StringCopy(altName + 8, imgName, MAX_FILENAME_LEN);

    /* if no name specified, delete it from the store */
    if(imgText == NULL)
    {
        /* try to find the graphic name in the ALTTEXT store */
        if (RemoveVariable(task->varstore, altName) == TRUE)
        {
            HtpMsg(MSG_INFO, task->infile, "ALT text for image \"%s\" removed",
                imgName);
        } else {
            /* tried to delete an image not already in the store */
            /* just post a warning */
            HtpMsg(MSG_WARNING, task->infile,
                   "attempted to delete image text not already defined");
        }
        return DISCARD_MARKUP;
    }

    if ((altNameCopy = DuplicateString(altName)) == NULL
        || (imgTextCopy = DuplicateString(imgText)) == NULL) {
        HtpMsg(MSG_ERROR, task->infile,
               "Can't duplicate text, out of Memory.");
        if (altNameCopy != NULL)
            FreeMemory(altNameCopy);
        return MARKUP_ERROR;
    }

    StoreVariable(task->varstore, altNameCopy, imgTextCopy, 
                  VAR_TYPE_ALTTEXT, 
                  VAR_FLAG_DEALLOC_NAME | VAR_FLAG_DEALLOC_VALUE, 
                  NULL, NULL);

    HtpMsg(MSG_INFO, task->infile, "ALT text for image \"%s\" set to \"%s\"",
        imgName, imgText);

    return DISCARD_MARKUP;
}

uint ImageUrlProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext)
{
    const char *url, *path;
    char *pathCopy;
 
    UNREF_PARAM(newPlaintext);

    /* get the relevant information */
    url = MarkupAttributeValue(htmlMarkup, "URL");

    /* requires at least a NAME parameter */
    if (url == NULL)
    {
        HtpMsg(MSG_ERROR, task->infile, 
               "IMAGEURL requires a nonempty URL attribute");
        return MARKUP_ERROR;
    }
    path = MarkupAttributeValue(htmlMarkup, "PATH");

    /* if no name specified, delete it from the store */
    if (path == NULL)
    {
        if (RemoveImageUrl(url)) {
            HtpMsg(MSG_INFO, task->infile, "path for image URL \"%s\" removed",
                   url);
        } else {
            /* tried to delete an image not already in the store */
            /* just post a warning */
            HtpMsg(MSG_WARNING, task->infile,
                   "attempted to delete URL path not already defined");
        }
        return DISCARD_MARKUP;
    }

    if ((pathCopy = ConvertDirDelimiter(path)) == NULL) {
        HtpMsg(MSG_ERROR, task->infile,
               "Can't duplicate path, out of Memory.");
        return MARKUP_ERROR;
    }

    StoreImageUrl(url, pathCopy);

    HtpMsg(MSG_INFO, task->infile, "path for URL \"%s\" set to \"%s\"",
           url, path);

    return DISCARD_MARKUP;
}

