/*
//
// gif.h
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef GIF_H
#define GIF_H

/* GIF format detected? */
BOOL GifFormatFound(FILE *file);

/* get image dimensions */
BOOL GifReadDimensions(FILE *file, DWORD *height, DWORD *width);

#endif

