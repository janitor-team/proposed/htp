/*
//
// gif.c
//
// GIF (Graphic Interchange Format) support functions
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#include "htp.h"

BOOL GifFormatFound(FILE *file)
{
    BYTE header[8];

    /* move to BOF */
    if(fseek(file, 0, SEEK_SET) != 0)
    {
        DEBUG_PRINT(("unable to seek to start of GIF file"));
        return FALSE;
    }

    /* read first six bytes, looking for GIF header + version info */
    if(fread(header, 1, 6, file) != 6)
    {
        DEBUG_PRINT(("could not read GIF image file header"));
        return FALSE;
    }

    /* is this a GIF file? */
    if((memcmp(header, "GIF87a", 6) == 0) || (memcmp(header, "GIF89a", 6) == 0))
    {
        return TRUE;
    }

    /* not a GIF file */
    return FALSE;
}

BOOL GifReadDimensions(FILE *file, DWORD *height, DWORD *width)
{
    BYTE buff[4];

    /* move to the image size position in the file header */
    if(fseek(file, 6, SEEK_SET) != 0)
    {
        DEBUG_PRINT(("unable to seek to start of GIF file"));
        return FALSE;
    }

    /* read width and height into a byte array */
    if(fread(buff, 1, 4, file) != 4)
    {
        DEBUG_PRINT(("unable to read word from JFIF file"));
        return FALSE;
    }

    /* this gets around machine endian problems while retaining the */
    /* fact that GIF uses little-endian notation */
    *width  = MAKE_WORD(buff[1], buff[0]);
    *height = MAKE_WORD(buff[3], buff[2]);
    return TRUE;
}

