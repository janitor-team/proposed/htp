/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
//
// misc-proc.h
//
// specialized markup processors
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef MISC_PROC_H
#define MISC_PROC_H

#include "defs.h"

uint HeadProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext);
uint QuoteProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext);

uint HtpCommentProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext);

uint ConditionalWarning(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext);

uint PreProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext);

#endif
