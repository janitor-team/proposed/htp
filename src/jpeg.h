/*
//
// jpeg.h
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef JPEG_H
#define JPEG_H

/* is this a JPEG File Interchange Format (JFIF) file? */
BOOL JpegFormatFound(FILE *file);

/* one-shot-does-it-all function */
BOOL JpegReadDimensions(FILE *file, DWORD *height, DWORD *width);

#endif

