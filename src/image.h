/*
//
// image.h
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef IMAGE_H
#define IMAGE_H

/*
// image file structure
*/
typedef struct tagIMAGEFILE
{
    char        *name;
    FILE        *file;
    uint        imageType;
} IMAGEFILE;

BOOL OpenImageFile(const char *filename, IMAGEFILE *imageFile);
void CloseImageFile(IMAGEFILE *imageFile);
BOOL GetImageDimensions(IMAGEFILE *imageFile, DWORD *width, DWORD *height);


/*
// image url substitution
*/

BOOL ExistsImageUrl (const char *url);
BOOL StoreImageUrl (const char *url, char* path);
BOOL RemoveImageUrl (const char *url);

#endif

