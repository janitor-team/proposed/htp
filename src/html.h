/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
//
// html.h
//
// HTML markup tag functions
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef MARKUP_H
#define MARKUP_H

#define MAX_ATTRIBUTE_COUNT     (32)

#define QUOTES_NONE   0
#define QUOTES_DOUBLE 1
#define QUOTES_SINGLE 2

typedef struct tagHTML_ATTRIBUTE
{
    char        *name;
    char        *value;
    int         quotes;
    char        *whitespace;
    struct tagHTML_ATTRIBUTE *next;
} HTML_ATTRIBUTE;

typedef struct tagHTML_MARKUP
{
    char        *tag;
    BOOL        single; /* single tag (xml-style) ending with "/>" */
    char        *whitespace;
    HTML_ATTRIBUTE *attrib;
} HTML_MARKUP;

/*
// functions
*/

/* whitespace functions are not specific to markup tags, but commonly used */
const char *FindWhitespace(const char *str);
const char *FindNonWhitespace(const char *str);

/* HTML_ATTRIBUTE functions */
BOOL ChangeAttributeName(HTML_ATTRIBUTE *htmlAttribute, char *name);
BOOL ChangeAttributeValue(HTML_ATTRIBUTE *htmlAttribute, char *value, 
                          int quotes);
void DestroyAttribute(HTML_ATTRIBUTE *htmlAttribute);

/* HTML_MARKUP functions */
BOOL PlaintextToMarkup(const char *plaintext, HTML_MARKUP *htmlMarkup);
BOOL MarkupToPlaintext(HTML_MARKUP *htmlMarkup, char **plaintext);
BOOL AddAttributeToMarkup(HTML_MARKUP *htmlMarkup, 
                          const char *name, const char *value, int quotes);
void DestroyMarkupStruct(HTML_MARKUP *htmlMarkup);
BOOL IsMarkupTag(HTML_MARKUP *htmlMarkup, const char *tag);
BOOL UnlinkBoolAttributeInMarkup(HTML_MARKUP *htmlMarkup, const char *name);
const char *MarkupAttributeValue(HTML_MARKUP *htmlMarkup, const char *name);
BOOL ChangeMarkupTag(HTML_MARKUP *htmlMarkup, char *tag);
HTML_ATTRIBUTE *UnlinkAttributeInMarkup(HTML_MARKUP *htmlMarkup, const char *name);

#endif

