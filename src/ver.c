/*
//
// ver.c
//
// version and program information
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#include "htp.h"

/*
// program usage & syntax
*/

void DisplayHeader(void)
{
    printf("\n%s %s - HTML pre-processor\n", PROGRAM_NAME, VER_STRING);
    printf("Copyright (c) 1995-96 Jim Nelson\n");
    printf("Copyright (c) 2002-14 Jochen Hoenicke under Artistic License\n");
    printf("[%s]\n\n", PROGRAM_OS);
}   

void usage(void)
{
    printf("Original author, Jim Nelson.\n");
    printf("email:\thtp-discuss@lists.sourceforge.net\n");
    printf("WWW:\thttp://htp.sourceforge.net/\n\n");
    printf("usage: %s [options] <source HTML file> <reformatted HTML file>\n",
        PROGRAM_NAME);
    printf("       %s [options] @<response file>\n", PROGRAM_NAME);
    printf("       %s [options] @\n", PROGRAM_NAME);
    printf("\nSee on-line reference for full details.\n");
    printf("\nThis is software is released under the Artistic License.\n");
}   

