/*
//
// msg.h
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef MSG_H
#define MSG_H

/*
// message severity levels
*/
#define MSG_ERROR               (2)
#define MSG_WARNING             (1)
#define MSG_INFO                (0)

/*
// set the printable message level ... if the message being written
// does not meet the level, it is not printed
*/
void SetMessageSeverityLevel(uint level);

/*
// error/warning/info message display function
*/
#ifndef DUMB_MSG_PROTOTYPE
void HtpMsg(uint severityLevel, STREAM *textFile, const char *format, ...);
#endif

#if DEBUG
/*
// debug message display, which also will dump to disk
*/
void DebugInit(const char *debugMsgFilename);
void DebugTerminate(void);

#ifndef DUMB_MSG_PROTOTYPE
void DebugMsg(const char *format, ...);
#endif
#endif

#endif

